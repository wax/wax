import { isMatch, first, last } from 'lodash'
import { SwitchTextTypeCommand } from 'substance'

import {
  createContainerSelection,
  containsList,
  isListItem,
  sliceListsAccordingToSelection,
  transactionHelpers,
  transformListToParagraphs,
} from '../helpers/ListHelpers'

class WaxSwitchTextTypeCommand extends SwitchTextTypeCommand {
  constructor(config) {
    super(config)
    if (!config.spec) {
      throw new Error("'config.spec' is mandatory")
    }
    if (!config.spec.type) {
      throw new Error("'config.spec.type' is mandatory")
    }
  }

  getType() {
    return this.config.spec.type
  }

  getCommandState(params) {
    let doc = params.editorSession.getDocument()
    let sel = params.selection
    let isBlurred = params.editorSession.isBlurred()

    let commandState = {
      disabled: false,
    }

    if (params.surface && !params.surface.context.editor.props.mode.styling) {
      commandState.disabled = true
      return commandState
    }

    if (params.surface && params.surface.containerId !== 'main') {
      commandState.disabled = true
      return commandState
    }

    if (sel.isPropertySelection() && !isBlurred) {
      let path = sel.getPath()
      let node = doc.get(path[0])
      if (node && node.isText() && node.isBlock()) {
        commandState.active = isMatch(node, this.config.spec)
        // When cursor is at beginning of a text block we signal
        // that we want the tool to appear contextually (e.g. in an overlay)
        commandState.showInContext =
          sel.start.offset === 0 && sel.end.offset === 0
      } else if (node.type === 'list-item') {
        commandState.disabled = false
      } else {
        commandState.disabled = true
      }
    } else {
      if (sel.isContainerSelection()) {
        commandState.disabled = false
      } else {
        commandState.disabled = true
      }
      // TODO: Allow Container Selections too, to switch multiple paragraphs
    }

    return commandState
  }

  /**
      Perform a switchTextType transformation based on the current selection
    */
  execute(params) {
    const { editorSession, surface } = transactionHelpers(params)
    if (!surface) {
      console.warn('No focused surface. Stopping command execution.')
      return
    }

    const selection = editorSession.getSelection()
    const isContainerSelection = selection.isContainerSelection()

    const selectionContainsList = containsList(params, selection)

    if (isContainerSelection) {
      if (selectionContainsList) {
        this.applyMixTransformations(params, selection)
      } else {
        this.applyPropertyTransformations(params, selection)
      }
      return
    }

    if (selectionContainsList) {
      this.applyMixTransformations(params, selection)
      return
    }

    editorSession.transaction(tx => {
      return tx.switchTextType(this.config.spec)
    })
  }

  applyPropertyTransformations(params, selection) {
    const { editorSession } = params

    editorSession.transaction(tx => {
      this.createPropertyTransformations(params, tx, selection)
    })
  }

  applyMixTransformations(params, selection) {
    const { editorSession } = transactionHelpers(params)
    const isStartSelectionAListItem = isListItem(
      params,
      selection.start.path[0],
    )
    const isEndSelectionAListItem = isListItem(params, selection.end.path[0])
    const lists = sliceListsAccordingToSelection(params, selection)

    editorSession.transaction(tx => {
      const options = { config: { orderd: false } }
      const paragraphs = transformListToParagraphs(params, tx, lists, options)

      const startPath = isStartSelectionAListItem
        ? paragraphs[0]
        : selection.start.path[0]

      const endPath = isEndSelectionAListItem
        ? last(paragraphs)
        : selection.end.path[0]

      const sel = createContainerSelection(
        params,
        selection,
        tx,
        startPath,
        endPath,
      )

      this.createPropertyTransformations(params, tx, sel)
    })
  }

  createPropertyTransformations(params, tx, selection) {
    const { containerId } = transactionHelpers(params)

    const nodeData = this.config.spec
    const newTransformations = []
    // Break selection spansning multiple nodes into pieces
    const propertySelections = selection.splitIntoPropertySelections()

    propertySelections.forEach(property => {
      // Make selection for block
      tx.setSelection({
        containerId,
        endOffset: property.end.offset,
        path: property.path,
        startOffset: property.start.offset,
        type: 'property',
      })

      // Apply style to block
      const transformation = tx.switchTextType(nodeData)

      // Keep new transformations ids to recreate selection
      newTransformations.push(transformation.id)
    })

    // Recreate Selection
    const firstBlock = first(newTransformations)
    const lastBlock = last(newTransformations)

    tx.setSelection({
      containerId,
      endPath: [lastBlock, 'content'],
      endOffset: selection.end.offset,
      startPath: [firstBlock, 'content'],
      startOffset: selection.start.offset,
      type: 'container',
    })
  }

  isSwitchTypeCommand() {
    return true
  }
}

export default WaxSwitchTextTypeCommand
