import { includes, some, forEach } from 'lodash'
import { AbstractEditor } from 'substance'

import BareLayout from './ui/layouts/Bare'
import EditoriaLayout from './ui/layouts/Editoria'
import Notification from './elements/notification/Notification'
// TODO -- css should maybe be with the layout
import './main.scss'

class Editor extends AbstractEditor {
  constructor(parent, props) {
    super(parent, props)

    this.handleActions({
      trackChangesViewToggle: this.trackChangesViewToggle,
      update: props.update,
    })

    //Toggle Modals
    this.on(
      'toggleModal',
      modal => {
        this.extendState({ [modal]: !this.state[modal] })
      },
      this,
    )
  }

  initializeProviders() {
    const ref = this
    const newContext = {}
    const providers = this.props.configurator.getProviders()
    Object.entries(providers).forEach(provider => {
      const name = provider[0]
      newContext[name] = provider[1].initialize(ref)
    })

    return newContext
  }

  trackChangesViewToggle() {
    this.extendState({
      trackChangesView: !this.state.trackChangesView,
    })
  }

  render($$) {
    const {
      chapterNumber,
      configurator,
      containerId,
      customTags,
      checkSpell,
      editing,
      layout,
      mode,
      onClose,
      trackChanges,
      update,
      autoSave,
      user,
      theme,
    } = this.props

    const {
      changesNotSaved,
      diacriticsModal,
      findAndReplaceModal,
      shortCutsModal,
      trackChangesView,
      spellCheck,
      fullScreen,
    } = this.state

    const containerProps = {
      chapterNumber,
      changesNotSaved,
      configurator,
      containerId,
      checkSpell,
      customTags,
      mode,
      diacriticsModal,
      findAndReplaceModal,
      shortCutsModal,
      fullScreen,
      editing,
      editorSession: this.editorSession,
      onClose,
      spellCheck,
      trackChanges,
      trackChangesView,
      autoSave,
      update,
      user,
      theme,
    }

    configurator.config.autoSave = autoSave
    let renderLayout

    const el = $$('div').addClass(`sc-prose-editor sc-layout-${layout}`)

    switch (layout) {
      case 'bare':
        renderLayout = $$(BareLayout, containerProps)
        break
      default:
        renderLayout = $$(EditoriaLayout, containerProps)
    }

    renderLayout.ref('layout')
    el.append(renderLayout)

    if (this.state.fullScreen) el.addClass('full-screen')

    el.append($$(Notification))
    return el
  }

  getInitialState() {
    return {
      trackChangesView: true,
      spellCheck: false,
      toggleInlineTag: false,
    }
  }

  // TODO -- move
  canToggleTrackChanges() {
    return true
  }

  scrollTo(nodeId) {
    const { layout } = this.refs
    const { contentPanel } = layout.refs
    contentPanel.scrollTo(nodeId)
  }

  triggerUnsavedModal(location, action) {
    if (this.editorSession.hasUnsavedChanges()) {
      this.extendState({ changesNotSaved: true })

      const { layout } = this.refs
      const modal = layout.refs['modal-warning']
      modal.extendProps({
        action,
        location,
      })
    }
  }

  // TODO -- set providers from the package
  getChildContext() {
    const oldContext = super.getChildContext()
    const newContext = this.initializeProviders()

    // attach all to context
    return Object.assign({ ...oldContext }, newContext)
  }

  dispose() {
    this.editorSession.dragManager.dispose()
  }
}

export default Editor
