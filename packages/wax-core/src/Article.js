import { Document } from 'substance'

class Article extends Document {
  _initialize() {
    super._initialize()
    this.create({
      type: 'container',
      id: 'main',
      nodes: [],
    })
  }
}

export default Article
