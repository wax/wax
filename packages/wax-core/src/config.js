import { BasePackage, ProseArticle } from 'substance'
import Article from './Article'
//setup
import MainNode from './setup/MainNode'
import MainConverter from './setup/MainConverter'
import defaultToolPanels from './setup/defaultToolPanels'
import ucpToolPanels from './setup/ucpToolPanels'
import bookSprintToolPanels from './setup/bookSprintToolPanels'

// IMPORT BLOCK lEVEL BODY PACKAGES
import AuthorPackage from './elements/block_level_body/author/AuthorPackage'
import BibliographyEntryPackage from './elements/block_level_body/bibliographyEntry/BibliographyEntryPackage'
import ChapterSubtitlePackage from './elements/block_level_body/chapterSubtitle/ChapterSubtitlePackage'
import ChapterTitlePackage from './elements/block_level_body/chapterTitle/ChapterTitlePackage'
import PartNumberPackage from './elements/block_level_body/part_number/PartNumberPackage'
import EpigraphPoetryPackage from './elements/block_level_body/epigraphPoetry/EpigraphPoetryPackage'
import EpigraphProsePackage from './elements/block_level_body/epigraphProse/EpigraphProsePackage'
import ExtractPoetryPackage from './elements/block_level_body/extractPoetry/ExtractPoetryPackage'
import ExtractProsePackage from './elements/block_level_body/extractProse/ExtractProsePackage'
import ParagraphContdPackage from './elements/block_level_body/paragraphContd/ParagraphContdPackage'
import ParagraphPackage from './elements/block_level_body/paragraph/ParagraphPackage'
import HeadingPackage from './elements/block_level_body/headings/HeadingPackage'
import SourceNotePackage from './elements/block_level_body/source_note/SourceNotePackage'

// IMPORT BLOCK lEVEL FRONT PACKAGES
import HalfTitlePackage from './elements/block_level_front/half_title/HalfTitlePackage'
import SeriesTitlePackage from './elements/block_level_front/series_title/SeriesTitlePackage'
import PublisherPackage from './elements/block_level_front/publisher/PublisherPackage'
import SeriesEditorPackage from './elements/block_level_front/series_editor/SeriesEditorPackage'
import DedicationPackage from './elements/block_level_front/dedication/DedicationPackage'
import SignaturePackage from './elements/block_level_front/signature/SignaturePackage'

// IMPORT BLOCK lEVEL BACK PACKAGES
import GlossaryPackage from './elements/block_level_back/glossary/GlossaryPackage'

import CustomTagBlockPackage from './elements/custom_tags_block/CustomTagBlockPackage'

// IMPORT ANNOTATION PACKAGES PACKAGES
import SmallCapsPackage from './elements/annotations/small_caps/SmallCapsPackage'
import SubscriptPackage from './elements/annotations/subscript/SubscriptPackage'
import SuperscriptPackage from './elements/annotations/superscript/SuperscriptPackage'
import StrongPackage from './elements/annotations/strong/StrongPackage'
import EmphasisPackage from './elements/annotations/emphasis/EmphasisPackage'
import CommentPackage from './elements/annotations/comment/CommentPackage'
import LinkPackage from './elements/annotations/link/LinkPackage'
import ScriptPackage from './elements/annotations/script/ScriptPackage'
import CodePackage from './elements/annotations/code/CodePackage'
import HighLighterPackage from './elements/annotations/highlighter/HighLighterPackage'
import TrackChangePackage from './elements/annotations/track_change/TrackChangePackage'
import CustomTagsPackage from './elements/custom_tags/CustomTagsPackage'
// My Elements
import PersistencePackage from './elements/persistence/PersistencePackage'
import DiacriticsPackage from './elements/diacritics/DiacriticsPackage'
import FindAndReplacePackage from './elements/find_and_replace/FindAndReplacePackage'
import ImagePackage from './elements/images/ImagePackage'
import NotePackage from './elements/note/NotePackage'
import NotePanePackage from './panes/Notes/NotePanePackage'
import SwitchSurfacePackage from './elements/switchsurface/SwitchSurfacePackage'
import FullScreenPackage from './elements/fullscreen/FullScreenPackage'
import ShortCutsModalPackage from './elements/shortcuts_modal/ShortCutsModalPackage'
import OrnamentPackage from './elements/ornament/OrnamentPackage'
import SpellCheckPackage from './elements/spellCheck/SpellCheckPackage'
import SpellCheckTogglePackage from './elements/spellcheck_toggle/SpellCheckTogglePackage'
import SpellCheckToggleLanguagesPackage from './elements/spellcheck_toggle_languages/SpellCheckToggleLanguagesPackage'
import ChangeCasePackage from './elements/change_case/ChangeCasePackage'
import QuoteMarksPackage from './elements/quote_mark/QuoteMarksPackage'
import SingleQuoteMarksPackage from './elements/single_quote_mark/SingleQuoteMarksPackage'
import EllipsesPackage from './elements/ellipses/EllipsesPackage'
import ListPackage from './elements/list/ListPackage'
import TablePackage from './elements/table/TablePackage'
import InlineNotePackage from './elements/inline_note/InlineNotePackage'
import CustomTagInlineTogglePackage from './elements/custom_tag_inline_toggle/CustomTagInlineTogglePackage'

const config = {
  name: 'simple-editor',
  configure: (config, options) => {
    config.defineSchema({
      name: 'prose-article',
      version: '1.0.0',
      DocumentClass: Article,
      defaultTextType: 'paragraph',
    })
    // Editor Nodes
    config.addNode(MainNode)
    config.addConverter('html', MainConverter)

    config.import(BasePackage, {
      noBaseStyles: options.noBaseStyles,
    })

    config.import(defaultToolPanels)
    config.import(ucpToolPanels)
    config.import(bookSprintToolPanels)

    // Block Level Packages
    config.import(HalfTitlePackage)
    config.import(ChapterTitlePackage)
    config.import(ChapterSubtitlePackage)
    // config.import(PartNumberPackage)
    config.import(AuthorPackage)
    config.import(PublisherPackage)
    config.import(SeriesTitlePackage)
    config.import(SeriesEditorPackage)
    config.import(DedicationPackage)
    config.import(ParagraphPackage)
    config.import(ParagraphContdPackage)
    config.import(ExtractProsePackage)
    config.import(ExtractPoetryPackage)
    config.import(EpigraphProsePackage)
    config.import(EpigraphPoetryPackage)
    config.import(BibliographyEntryPackage)
    config.import(SignaturePackage)
    config.import(SourceNotePackage)
    config.import(HeadingPackage)
    config.import(GlossaryPackage)

    config.import(TablePackage)
    config.import(StrongPackage)
    config.import(EmphasisPackage)
    config.import(SmallCapsPackage)
    config.import(SubscriptPackage)
    config.import(SuperscriptPackage)
    config.import(ScriptPackage)
    config.import(LinkPackage)
    config.import(PersistencePackage)
    config.import(CodePackage)
    config.import(OrnamentPackage)
    config.import(CommentPackage)
    config.import(ImagePackage)
    config.import(ListPackage)
    config.import(NotePackage)
    config.import(NotePanePackage)
    config.import(SpellCheckPackage)
    config.import(TrackChangePackage)
    config.import(DiacriticsPackage)
    config.import(FindAndReplacePackage)
    config.import(SwitchSurfacePackage)
    config.import(HighLighterPackage)
    config.import(FullScreenPackage)
    config.import(ShortCutsModalPackage)
    config.import(SpellCheckTogglePackage)
    config.import(SpellCheckToggleLanguagesPackage)
    config.import(QuoteMarksPackage)
    config.import(SingleQuoteMarksPackage)
    config.import(EllipsesPackage)
    config.import(ChangeCasePackage)

    config.import(InlineNotePackage)
    config.import(CustomTagsPackage)
    config.import(CustomTagBlockPackage)
    config.import(CustomTagInlineTogglePackage)
  },
}

export default config
