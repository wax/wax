import { forEach } from 'lodash'
import { EditorSession as SubstanceEditorSession } from 'substance'
import { documentHelpers } from 'substance'

class EditorSession extends SubstanceEditorSession {
  constructor(...props) {
    super(...props)
    this.autoSaveFlag = true
    this.onUpdate('', this.autoSave, this)
  }

  undo() {
    super.undo()
    const notes = documentHelpers.getPropertyAnnotationsForSelection(
      this.getDocument(),
      this.getSelection(),
      { type: 'note' },
    )
    if (notes.length >= 1) {
      forEach(notes, note => {
        if (note.disabled) {
          note.disabled = false
          this.editor.emit('updateNotes')
        }
      })
    }
  }

  autoSave() {
    if (
      !this.configurator.config.autoSave ||
      !this.autoSaveFlag ||
      !this._hasUnsavedChanges
    )
      return
    this.autoSaveFlag = false
    setTimeout(() => {
      this.save()
      this.autoSaveFlag = true
      this.editor.emit('hightLightSave')
    }, 10000)
  }

  save() {
    const saveHandler = this.saveHandler
    this.document.emit('document:saved')
    if (this._hasUnsavedChanges && !this._isSaving) {
      this._isSaving = true
      // Pass saving logic to the user defined callback if available
      if (saveHandler) {
        const saveParams = {
          editorSession: this,
          fileManager: this.fileManager,
        }
        return saveHandler
          .saveDocument(saveParams)
          .then(() => {
            this._hasUnsavedChanges = false
            // We update the selection, just so a selection update flow is
            // triggered (which will update the save tool)
            // TODO: model this kind of update more explicitly. It could be an 'update' to the
            // document resource (hasChanges was modified)
            if (!this.configurator.config.autoSave) {
              this.setSelection(this.getSelection())
            }
          })
          .catch(err => {
            console.error('Error during save', err)
          })
          .then(() => {
            // finally
            this._isSaving = false
          })
      }
      console.error(
        'Document saving is not handled at the moment. Make sure saveHandler instance provided to editorSession',
      )
      return Promise.reject()
    }
  }
}

export default EditorSession
