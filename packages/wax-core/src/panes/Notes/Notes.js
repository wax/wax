/* eslint react/prop-types: 0 */
import SimpleBar from 'simplebar'

import { map, forEach, values, isEmpty } from 'lodash'
import { Component, Overlay, ScrollPane, SplitPane } from 'substance'

import Comments from '../Comments/CommentBoxList'
import NoteContainerComponent from './NoteContainerComponent'

class Notes extends Component {
  constructor(...props) {
    super(...props)

    this.resize = this.resize.bind(this)
    this.stopResize = this.stopResize.bind(this)
    this.dataDeleted = []
    this.notesPaneHeight = 135
  }

  didMount() {
    const notesProvider = this.getProvider()
    notesProvider.on('notes:updated', this.updateContainers, this)
    notesProvider.on('note:dragged', this.renumber, this)
    this.computeMainEditorPane()
    // setTimeout(() => {
    //   this.notesContentPanel = new SimpleBar(
    //     document.getElementById('content-panel-notes'),
    //     {
    //       autoHide: false,
    //     },
    //   )
    // })
  }

  updateContainers(data) {
    if (!data) return // TODO REMOVE (this exists only because events are registed twice)
    const provider = this.getProvider()
    const notes = provider.entries
    const doc = this.context.editorSession.document

    forEach(data.created, value => {
      doc.create({ type: 'note-container', id: `container-${value.id}` })

      const noteExistsInDeleted = values(this.dataDeleted).filter(
        val => val.id === value.id,
      )

      if (!isEmpty(noteExistsInDeleted)) {
        const newContainer = doc.get(`container-${value.id}`)
        forEach(this.dataDeleted, v => {
          if (v.id === value.id) {
            doc.delete(newContainer.id)
            doc.create({
              type: 'note-container',
              id: `container-${value.id}`,
              nodes: v.nodes,
            })
          }
        })
      }
    })

    forEach(notes, (value, order) => {
      const container = doc.get(`container-${value.id}`)
      container.order = order + 1
    })

    // IN case of deleted note(s) keep all paragraphs id's linked
    // to the container created for the note
    forEach(data.deleted, value => {
      const selection = this.context.editorSession.getSelection()
      const containerToBeDeleted = doc.get(`container-${value.id}`)
      const obj = { id: value.id, nodes: containerToBeDeleted.nodes }
      this.dataDeleted.push(obj)
      doc.delete(containerToBeDeleted.id)
    })
    this.rerender()
  }

  renumber() {
    const provider = this.getProvider()
    const notes = provider.entries
    const doc = this.context.editorSession.document
    forEach(notes, (value, order) => {
      const container = doc.get(`container-${value.id}`)
      container.order = order + 1
    })
    this.rerender()
  }

  didUpdate() {
    this.el.el.style.height = `${this.notesPaneHeight}px`
    this.computeMainEditorPane()
    // this.notesContentPanel.init()
    // this.notesContentPanel.initDOM()
  }

  render($$) {
    const { editing, trackChanges, user, configurator } = this.props

    const provider = this.getProvider()
    const notes = provider.entries

    const title = $$('span').append('Notes')

    const titleContainer = $$('div')
      .addClass('notes-title')
      .append(title)

    const resizer = $$('div').addClass('resize-area')
    resizer.addEventListener('mousedown', this.initResize)

    let height = 0
    let display = 'none'
    if (notes.length > 0) {
      height = 135
      display = 'block'
    }

    const el = $$('div')
      .addClass('notes-container')
      .attr('style', `height: ${height}px; display: ${display}`)
      .append(titleContainer)
      .append(resizer)

    const containerNotes = map(notes, (note, i) => {
      const containerId = `container-${note.id}`
      const doc = this.context.editorSession.document
      const { editor } = this.context

      if (!doc.get(`container-${note.id}`)) {
        doc.create({
          type: 'note-container',
          id: `container-${note.id}`,
          nodes: [],
        })
        setTimeout(() => {
          editor.emit('displayNotification', () => {
            return `Could not find note. Creating a blank one.`
          })
        }, 3000)
      }
      const noteItem = $$(NoteContainerComponent, {
        containerId,
        disabled: note.node.disabled,
        editing,
        trackChanges,
        noteParentId: note.id,
        order: i + 1,
      }).ref(`container-${note.id}`)

      return noteItem
    })

    const commentsPaneNotes = $$(Comments, {
      user,
      containerType: 'note-container',
    }).addClass('sc-comments-pane')

    const noteItemsWithCommentsPane = $$(SplitPane, {
      sizeA: '50%',
      splitType: 'vertical',
    }).append($$('div').append(containerNotes), commentsPaneNotes)

    const { overlay } = configurator.menus
    const overlayMenu = overlay ? overlay : 'defaultOverlay'

    const contentPanel = $$(ScrollPane, {
      name: 'contentPanelNotes',
      scrollbarPosition: 'right',
    })
      .append(
        noteItemsWithCommentsPane,
        $$(Overlay, {
          toolPanel: configurator.getToolPanel(overlayMenu),
          theme: 'dark',
        }),
      )
      .attr('id', `content-panel-notes`)
      .ref('contentPanelNotes')

    return el.append(contentPanel)
  }

  initResize() {
    window.addEventListener('mousemove', this.resize, false)
    window.addEventListener('mouseup', this.stopResize, false)
  }

  // TODO -- should be cleaner
  resize(e) {
    const elementOffset = this.el.el.getBoundingClientRect().top
    this.notesPaneHeight = this.el.el.offsetHeight + elementOffset - e.clientY
    this.el.el.style.height = `${this.notesPaneHeight}px`
    this.computeMainEditorPane()
  }

  stopResize(e) {
    window.removeEventListener('mousemove', this.resize, false)
    window.removeEventListener('mouseup', this.stopResize, false)
  }

  computeMainEditorPane() {
    const { containerId } = this.context.editor.props
    const mainPane = document.getElementById(`content-panel-${containerId}`)
    mainPane.style.height =
      this.el.el.offsetTop === 0 ? `97%` : `${this.el.el.offsetTop - 15}px`
  }

  getProvider() {
    return this.context.notesProvider
  }

  // // TODO -- do we need this?
  // dispose () {
  //   // const provider = this.getProvider()
  //   // provider.off(this)
  // }
}

export default Notes
