import { forEach, last, debounce } from 'lodash'
import { Component, PropertySelection } from 'substance'

import {
  containsList,
  getNodesForSelection,
  sliceListsAccordingToSelection,
} from '../../helpers/ListHelpers'

class WordCountComponent extends Component {
  constructor(...props) {
    super(...props)
    this.wordCounter = 0
  }

  didMount() {
    this.context.editorSession.onUpdate('', debounce(this.getString, 500), this)
    this.context.notesProvider.on('notes:updated', this.rerender, this)
    this.context.commentsProvider.on('comments:updated', this.rerender, this)
  }
  render($$) {
    const notesCounter = this.context.notesProvider.entries.length
    const commentsCounter = this.context.commentsProvider.entries.length
    const trackChangesCounter = this.context.trackChangesProvider.getEntries()
      .length

    const el = $$('div').addClass('info-panel-inner')
    const words = $$('span')
      .addClass('word-counter')
      .append(`${this.wordCounter} words`)
    const notes = $$('span')
      .addClass('notes-counter')
      .append(`${notesCounter} notes`)
    const comments = $$('span')
      .addClass('comments-counter')
      .append(`${commentsCounter} comments`)
    const trackChanges = $$('span')
      .addClass('track-changes-counter')
      .append(`${trackChangesCounter} track changes`)
    el.append(words, notes, comments, trackChanges)
    return el
  }

  getString() {
    const { trackChanges } = this.context.editor.props
    const { editorSession } = this.context
    const selection = editorSession.getSelection()
    const doc = editorSession.getDocument()
    const params = {
      surface: { containerId: selection.containerId },
      editorSession,
    }
    this.wordCounter = 0

    if (
      selection.isCollapsed() &&
      !trackChanges &&
      this.wordCounter === this.state.wordCounter
    )
      return

    if (selection.isContainerSelection()) {
      const selectionContainsList = containsList(params, selection)
      if (selectionContainsList) {
        this.wordCounter = this.countWordsWithLists(params, selection, doc)
      } else {
        const propertySelections = selection.splitIntoPropertySelections()
        propertySelections.forEach(property => {
          this.wordCounter += this.countWords(property)
        })
      }
    } else {
      this.wordCounter += this.countWords(selection)
    }
    this.extendState({ wordCounter: this.wordCounter })
  }

  countWordsWithLists(params, selection, doc) {
    const allNodes = getNodesForSelection(params, selection)
    const listNodes = sliceListsAccordingToSelection(params, selection)
    const propertySelections = []
    const startPath = selection.start.path[0]
    const endPath = selection.end.path[0]

    allNodes.forEach(node => {
      if (node.content) {
        const startOffset = node.id === startPath ? selection.start.offset : 0
        const endOffset =
          node.id === endPath ? selection.end.offset : node.content.length
        const sel = new PropertySelection(
          [node.id, 'content'],
          startOffset,
          endOffset,
          false,
          'main',
          'main',
        )
        propertySelections.push(sel)
      }
    })
    listNodes.forEach(listNode => {
      for (let i = 1; i <= listNode.items.length; i++) {
        const listItem = doc.get(listNode.items[i - 1])
        const startOffset =
          listItem.id === startPath ? selection.start.offset : 0
        const endOffset =
          listItem.id === endPath
            ? selection.end.offset
            : listItem.content.length
        const sel = new PropertySelection(
          [listItem.id, 'content'],
          startOffset,
          endOffset,
          false,
          'main',
          'main',
        )
        propertySelections.push(sel)
      }
    })
    let counter = 0
    propertySelections.forEach(property => {
      counter += this.countWords(property)
    })
    return counter
  }

  countWords(selection) {
    if (selection.isNull() || selection.type === 'node') return 0
    const doc = this.context.editorSession.document
    const startOffset = selection.start.offset
    const endOffset = selection.end.offset
    const node = doc.get(selection.path[0])
    const actualString = node.content.substring(startOffset, endOffset)
    let wordCount = 0

    const separators = [' ', '-', '/', ':']

    forEach(actualString.split(new RegExp(separators.join('|'), 'g')), arr => {
      if (arr !== '' && !/^[^\w!?]+$/.test(arr)) wordCount += 1
    })

    if (startOffset > 0) {
      const nextFirstLetter = node.content.substring(
        startOffset - 1,
        startOffset,
      )

      const firstLetter = node.content.substring(startOffset, startOffset + 1)
      if (
        nextFirstLetter !== ' ' &&
        firstLetter !== ' ' &&
        !/^[-/,!@#$%^&*().;“‘… ]+$/.test(nextFirstLetter) &&
        !/^[-/,!@#$%^&*().;“‘… ]+$/.test(firstLetter)
      ) {
        wordCount -= 1
      }
    }

    const lastNextLetter = node.content.substring(endOffset, endOffset + 1)
    const lastLetter = node.content.substring(endOffset, endOffset - 1)

    if (
      lastNextLetter !== ' ' &&
      lastLetter !== ' ' &&
      endOffset !== node.content.length &&
      !/^[-/,!@#$%^&*().;”’… ]+$/.test(lastNextLetter) &&
      !/^[-/,!@#$%^&*().;”’… ]+$/.test(lastLetter)
    ) {
      wordCount -= 1
    }

    wordCount = wordCount < 0 ? 0 : wordCount
    return wordCount
  }
  getInitialState() {
    return {
      wordCounter: 0,
    }
  }
}

export default WordCountComponent
