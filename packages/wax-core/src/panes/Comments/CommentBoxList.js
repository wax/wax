/* eslint react/prop-types: 0 */

import { each, debounce, groupBy } from 'lodash'
import { Component } from 'substance'

import CommentBox from './CommentBox'

class CommentBoxList extends Component {
  constructor(props, args) {
    super(props, args)
    const provider = this.getProvider()
    this.totalComments = provider.entries.length
    this.newActiveComment = false
    this.savedActiveEntry = null
    this.setTops = this.setTops.bind(this)
    this.tops = []
  }

  didMount() {
    this.context.editor.on('newComment', this.onNewCommentEntry, this)
    this.context.editor.on('ui:updated', this.setTops, this)

    const provider = this.getProvider()
    provider.on('comments:updated', this.onCommentsUpdated, this)
    provider.on('commentReply:updated', this.rerender, this)
    this.context.editorSession.onRender(
      'document',
      this.getCommentEntries,
      this,
    )

    this.setTops()
  }

  didUpdate() {
    this.setTops()
  }

  getCommentEntries() {
    const provider = this.getProvider()
    const activeEntry = provider.activeEntry
    if (activeEntry !== this.savedActiveEntry && !this.newActiveComment) return
    const totalEntries = provider.getEntriesLengthByType('comment')

    if (totalEntries !== this.totalComments && !this.newActiveComment) {
      this.totalComments = totalEntries
      this.rerender()
      return
    }
    this.setTops()
    this.newActiveComment = false
  }

  render($$) {
    const provider = this.getProvider()
    const entries = provider.entries
    const activeEntry = provider.activeEntry
    const { user, containerType } = this.props
    const doc = this.context.editorSession.document

    const listItems = entries.map((entry, i) => {
      const active = entry.id === activeEntry
      if (containerType === entry.containerType && doc.get(entry.containerId)) {
        return $$(CommentBox, {
          active,
          discussion: entry.node.discussion,
          entry,
          setTops: this.setTops,
          user,
        })
      }
    })

    return $$('ul')
      .addClass('sc-comment-pane-list')
      .append(listItems)
  }

  setTops() {
    const provider = this.getProvider()
    const entries = provider.entries

    const activeEntry = provider.activeEntry

    this.calculateTops(groupBy(entries, 'containerType'), activeEntry)
  }

  calculateTops(entries, active) {
    const result = []
    const boxes = []

    each(entries[this.props.containerType], (entry, pos) => {
      // initialize annotationTop and boxHeight, as there is a chance that
      // annotation and box elements are not found by the selector
      // (as in when creating a new comment)
      let annotationTop = 0
      let boxHeight = 0
      let top = 0

      let isActive = false
      if (entry.id === active) isActive = true

      // get position of annotation in editor
      const annotationEl = document.querySelector(`span[data-id="${entry.id}"]`)

      const containerId = this.props.containerId
      const surfaceElementTop = this.props.containerId
        ? document
            .querySelector(`[data-surface-id="${containerId}"]`)
            .getBoundingClientRect().top
        : 800
      if (annotationEl) {
        annotationTop = this.props.containerId
          ? annotationEl.getBoundingClientRect().top - surfaceElementTop
          : annotationEl.offsetTop
      }

      // get height of this comment box
      const boxEl = document.querySelector(`li[data-comment="${entry.id}"]`)
      if (boxEl) boxHeight = parseInt(boxEl.offsetHeight)

      // keep the elements to add the tops to at the end
      boxes.push(boxEl)

      // where the box should move to
      top = annotationTop

      // if the above comment box has already taken up the height, move down
      if (pos > 0) {
        const previousBox = entries[this.props.containerType][pos - 1]
        const previousEndHeight = previousBox.endHeight
        if (annotationTop < previousEndHeight) {
          top = previousEndHeight + 2
        }
      }

      // store where the box ends to be aware of overlaps in the next box
      entry.endHeight = top + boxHeight + 2
      result[pos] = top

      // if active, move as many boxes above as needed to bring it to the annotation's height
      if (isActive) {
        entry.endHeight = annotationTop + boxHeight + 2
        result[pos] = annotationTop

        let b = true
        let i = pos

        // first one active, none above
        if (i === 0) b = false

        while (b) {
          const boxAbove = entries[this.props.containerType][i - 1]
          const boxAboveEnds = boxAbove.endHeight
          const currentTop = result[i]

          const doesOverlap = boxAboveEnds > currentTop
          if (doesOverlap) {
            const overlap = boxAboveEnds - currentTop
            result[i - 1] -= overlap
          }

          if (!doesOverlap) b = false
          if (i <= 1) b = false
          i -= 1
        }
      }
    })

    // give each box the correct top
    each(boxes, (box, i) => {
      const val = `${result[i]}px`
      if (box) {
        box.style.top = val
        box.style.transition = 'all 1s'
      }
    })
  }

  getProvider() {
    return this.context.commentsProvider
  }

  onCommentsUpdated() {
    const provider = this.getProvider()
    const activeEntry = provider.activeEntry
    if (activeEntry !== this.savedActiveEntry) {
      this.rerender()
      this.savedActiveEntry = activeEntry
    }
  }

  onNewCommentEntry() {
    this.newActiveComment = true
  }
}

export default CommentBoxList
