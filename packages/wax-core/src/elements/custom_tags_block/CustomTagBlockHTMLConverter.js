export default {
  type: 'custom-block',
  tagName: 'custom-block',

  import: (el, node, converter) => {
    node.content = converter.annotatedText(el, [node.id, 'content'])
    node.class = el
      .attr('class')
      .split('-')
      .join(' ')
  },

  export: (node, el, converter) => {
    el.setAttribute('class', node.class.split(' ').join('-'))
    el.append(converter.annotatedText([node.id, 'content']))
  },
}
