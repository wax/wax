import { TextBlockComponent } from 'substance'

class CustomTagBlockComponent extends TextBlockComponent {
  render($$) {
    const el = super.render($$)
    return el.addClass(this.props.node.class)
  }
}

export default CustomTagBlockComponent
