import CustomTagBlock from './CustomTagBlock'
import CustomTagBlockComponent from './CustomTagBlockComponent'
import CustomTagBlockHTMLConverter from './CustomTagBlockHTMLConverter'
import CustomTagBlockTool from './CustomTagBlockTool'
import WaxSwitchTextTypeCommand from '../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'custom-tag-block',
  configure: function(config) {
    config.addNode(CustomTagBlock)
    config.addComponent(CustomTagBlock.type, CustomTagBlockComponent)
    config.addConverter('html', CustomTagBlockHTMLConverter)
    config.addCommand('custom-tag-block', WaxSwitchTextTypeCommand, {
      spec: { type: 'custom-block' },
      commandGroup: 'custom-tag-block',
    })
    config.addLabel('custom-tag-block', {
      en: 'Custom Tag Block',
    })

    config.addTool('custom-tag-block', CustomTagBlockTool)
  },
  CustomTagBlock: CustomTagBlock,
  CustomTagBlockComponent: CustomTagBlockComponent,
  CustomTagBlockHTMLConverter: CustomTagBlockHTMLConverter,
}
