import { TextBlock } from 'substance'

class CustomTagBlock extends TextBlock {}

CustomTagBlock.schema = {
  type: 'custom-block',
  class: {
    type: 'string',
    default: '',
  },
}

export default CustomTagBlock
