import { PropertyAnnotation } from 'substance'

class FindAndReplace extends PropertyAnnotation {}

FindAndReplace.type = 'find-and-replace'

export default FindAndReplace
