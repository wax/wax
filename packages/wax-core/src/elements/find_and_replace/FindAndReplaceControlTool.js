import { Tool } from 'substance'

class FindAndReplaceControlTool extends Tool {}
FindAndReplaceControlTool.type = 'find-and-replace-tool'

export default FindAndReplaceControlTool
