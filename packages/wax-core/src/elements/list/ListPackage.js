import ListNode from './ListNode'
import ListItemNode from './ListItemNode'
import ListComponent from './ListComponent'
import ListItemComponent from './ListItemComponent'
import ListHTMLConverter from './ListHTMLConverter'
import ListItemHTMLConverter from './ListItemHTMLConverter'
import ToggleListCommand from './ToggleListCommand'
import UCPListCommand from './UCPListCommand'
import IndentListCommand from './IndentListCommand'

export default {
  name: 'list',
  configure: function(config) {
    config.addNode(ListNode)
    config.addNode(ListItemNode)
    config.addComponent('list', ListComponent)
    config.addComponent('list-item', ListItemComponent)
    config.addConverter('html', ListHTMLConverter)
    config.addConverter('html', ListItemHTMLConverter)

    config.addCommand('toggle-unordered-list', ToggleListCommand, {
      spec: { listType: 'bullet' },
      commandGroup: 'list',
    })
    //UCP BULLET
    config.addCommand('toggle-ucp-unordered-list', UCPListCommand, {
      spec: { listType: 'bullet' },
      commandGroup: 'ucp-list',
    })

    config.addLabel('toggle-unordered-list', {
      en: 'Toggle list',
    })

    //UCP BULLET DESCRIPTION
    config.addLabel('toggle-ucp-unordered-list', {
      en: 'Bulleted list',
    })

    config.addIcon('toggle-unordered-list', { fontawesome: 'fa-list-ul' })

    config.addCommand('toggle-ordered-list', ToggleListCommand, {
      spec: { listType: 'order' },
      commandGroup: 'list',
    })

    config.addCommand('toggle-ucp-ordered-list', UCPListCommand, {
      spec: { listType: 'order' },
      commandGroup: 'ucp-list',
    })

    config.addLabel('toggle-ordered-list', {
      en: 'Toggle list',
    })

    config.addLabel('toggle-ucp-ordered-list', {
      en: 'Numbered list',
    })

    config.addIcon('toggle-ordered-list', { fontawesome: 'fa-list-ol' })

    //QA

    config.addCommand('insert-unnumbered-list', UCPListCommand, {
      spec: { listType: 'un' },
      custom: 'in',
      commandGroup: 'ucp-list',
    })

    config.addLabel('insert-unnumbered-list', {
      en: 'Unnumbered list',
    })

    //QA

    config.addCommand('insert-qa-list', UCPListCommand, {
      spec: { listType: 'qa' },
      custom: 'qa',
      commandGroup: 'ucp-list-part-chapter',
    })

    config.addLabel('insert-qa-list', {
      en: 'Dialogue',
    })

    //Series List

    config.addCommand('insert-series-list', UCPListCommand, {
      spec: { listType: 'order' },
      custom: 'series',
      commandGroup: 'ucp-list-series',
    })

    config.addLabel('insert-series-list', {
      en: 'Series List',
    })

    //Abbreviations List

    config.addCommand('insert-abbreviations-list', UCPListCommand, {
      spec: { listType: 'abbreviations' },
      custom: 'abbreviations',
      commandGroup: 'ucp-list-abbreviations',
    })

    config.addLabel('insert-abbreviations-list', {
      en: 'Abbreviations',
    })

    config.addCommand('indent-list', IndentListCommand, {
      spec: { action: 'indent' },
      commandGroup: 'list',
    })

    config.addLabel('indent-list', {
      en: 'Increase indentation',
    })
    config.addIcon('indent-list', { fontawesome: 'fa-indent' })

    config.addCommand('dedent-list', IndentListCommand, {
      spec: { action: 'dedent' },
      commandGroup: 'list',
    })
    config.addLabel('dedent-list', {
      en: 'Decrease indentation',
    })
    config.addIcon('dedent-list', { fontawesome: 'fa-dedent' })
  },
  ListNode,
  ListItemNode,
  ListComponent,
  ListHTMLConverter,
  ListItemHTMLConverter,
  ToggleListCommand,
  IndentListCommand,
}
