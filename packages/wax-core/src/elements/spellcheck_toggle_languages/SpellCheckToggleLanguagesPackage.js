import { platform, Tool } from 'substance'
import SpellCheckToggleLanguagesCommand from './SpellCheckToggleLanguagesCommand'
import SpellCheckToggleLanguagesTool from './SpellCheckToggleLanguagesTool'

export default {
  name: 'spell-check-toggle-languages',
  configure: config => {
    config.addCommand(
      'spell-check-toggle-languages',
      SpellCheckToggleLanguagesCommand,
      {
        commandGroup: 'spell-check-toggle-languages',
      },
    )
    config.addTool(
      'spell-check-toggle-languages',
      SpellCheckToggleLanguagesTool,
    )
    config.addIcon('spell-check-toggle-languages', { fontawesome: 'fa-check' })
    config.addLabel('spell-check-toggle-languages', {
      en: 'Toggle spell check',
    })

    let controllerKey = 'ctrl'
    if (platform.isMac) controllerKey = 'cmd'
  },
}
