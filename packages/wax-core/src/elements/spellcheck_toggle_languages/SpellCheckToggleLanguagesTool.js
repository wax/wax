import { forEach, find } from 'lodash'
import { Tool, FontAwesomeIcon as Icon, documentHelpers } from 'substance'
import SpellCheckManager from '../spellCheck/SpellCheckManager'

class SpellCheckToggleTool extends Tool {
  constructor(...args) {
    super(...args)
    this.DropDownOptions = [
      {
        language: 'English',
        code: 'en-US',
      },
      {
        language: 'French',
        code: 'fr',
      },
      {
        language: 'German',
        code: 'de-DE',
      },
      {
        language: 'Portuguese',
        code: 'pt-PT',
      },
      {
        language: 'Spanish',
        code: 'es',
      },
      {
        language: 'Russian',
        code: 'ru-RU',
      },
    ]
  }

  render($$) {
    const editorSpellLang = this.context.editor.props.spellCheckLang

    const dropDownCarret = $$(Icon, { icon: 'fa-caret-down' }).addClass(
      'icon-down',
    )
    const title =
      editorSpellLang !== ''
        ? find(this.DropDownOptions, ['code', editorSpellLang]).language
        : 'Spell Check'

    const activeClass = editorSpellLang !== '' ? 'lang-active' : ''

    let el = $$('div')
      .addClass('change-language-wrapper')
      .attr('id', 'change-language')
      .append(
        $$('div')
          .addClass('choose-language')
          .addClass(activeClass)
          .attr('title', `Change Language`)
          .append($$('span').append(title))
          .append(dropDownCarret)
          .on('click', this.toggleDropdown),
      )
    let options = $$('span')
    if (this.state.open) {
      el.addClass('sm-open')

      // dropdown options
      options = $$('div')
        .addClass('se-options')
        .ref('options')
      forEach(this.DropDownOptions, (option, index) => {
        let button = $$('button')
          .addClass('se-option')
          .attr('data-type', option)
          .append(
            $$('div')
              .append(option.language)
              .addClass('option-box')
              .attr('title', option.language),
          )
          .on('click', () => this.setLanguage(option.language, option.code))
        options.append(button)
      })
    }
    return el.append(options)
  }

  setLanguage(language, code) {
    const editorProps = this.context.editor.props
    const { editorSession } = this.context

    this.context.editor.extendProps({
      spellCheckLang: code,
    })

    this.context.editor.extendState({ spellCheck: true })

    this.context.editor.emit('displayNotification', () => {
      return `Scanning the document for errors. Please Wait.`
    })

    const spellCheck = new SpellCheckManager(editorSession)
    spellCheck.runGlobalCheck()
  }

  toggleDropdown() {
    this.extendState({
      open: !this.state.open,
    })
  }

  getInitialState() {
    return {
      open: false,
    }
  }
}

export default SpellCheckToggleTool
