import { Command } from 'substance'

class DiacriticsCommand extends Command {
  getCommandState(params) {
    const sel = params.selection
    const { surface } = params
    const newState = {
      active: false,
      disabled: true,
    }

    if (surface && surface.props.editing === 'selection') {
      newState.disabled = true
      return newState
    }

    if (
      sel &&
      !sel.isNull() &&
      !sel.isCustomSelection() &&
      surface &&
      surface.isContainerEditor()
    ) {
      newState.disabled = false
    }
    return newState
  }

  execute(params) {
    const surface = params.editorSession.surfaceManager.getSurface('main')
    surface.context.editor.emit('toggleModal', 'diacriticsModal')
  }
}

DiacriticsCommand.type = 'diacritics-tool'

export default DiacriticsCommand
