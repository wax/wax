import { Tool } from 'substance'

class TrackChangeNextControlTool extends Tool {
  getClassNames() {
    return 'track-change-next'
  }

  renderButton($$) {
    const el = super.renderButton($$)

    const iconNext = $$('span').addClass('track-changes-icon-next')

    el.append('Next', iconNext)

    return el
  }
}

TrackChangeNextControlTool.type = 'track-change-next'

export default TrackChangeNextControlTool
