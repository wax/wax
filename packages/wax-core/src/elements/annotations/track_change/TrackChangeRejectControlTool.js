import { Tool } from 'substance'

class TrackChangeRejectControlTool extends Tool {
  getClassNames() {
    return 'track-change-reject'
  }

  renderButton($$) {
    const el = super.renderButton($$)
    el.append('Reject')

    return el
  }
}

TrackChangeRejectControlTool.type = 'track-change-reject'

export default TrackChangeRejectControlTool
