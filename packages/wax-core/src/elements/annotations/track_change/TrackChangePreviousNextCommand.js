import { Command } from 'substance'

class TrackChangePreviousNextCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: true,
    }

    if (!params.surface) {
      return newState
    }

    const editorProps = params.surface.context.editor.props
    if (editorProps.editing === 'selection') return newState

    const provider = this.getProvider(params)
    const entries = provider.computeEntries()

    if (entries.length === 0) return newState

    // TODO -- add another case
    // if there is only one entry && the selection is a change
    // disable previous / next
    // (because there is nowhere to go)

    newState = {
      active: true,
      disabled: false,
    }

    return newState
  }

  getCurrentSelection(params) {
    const editorSession = params.editorSession
    const selection = editorSession.getSelection()
    return selection
  }

  execute(params) {
    const provider = this.getProvider(params)
    const itemToFocus = this.getNextOrPrevious(params)
    provider.focus(itemToFocus)
  }

  getNextOrPrevious(params) {
    const provider = this.getProvider(params)
    const actionType = this.getActionType()

    if (actionType === 'previous') {
      return provider.getPreviousNode()
    }

    if (actionType === 'next') {
      return provider.getNextNode()
    }
    return false
  }

  // TODO -- use nodeType to get providers
  getProvider(params) {
    return params.surface.context.trackChangesProvider
  }

  getAnnotationType() {
    return this.config.nodeType
  }

  getActionType() {
    return this.config.actionType
  }
}

export default TrackChangePreviousNextCommand
