import { InsertNodeCommand } from 'substance'

class InsertScriptCommand extends InsertNodeCommand {
  getCommandState(params, context) {
    let sel = params.selection
    let newState = {
      disabled: true,
      active: false,
    }
    const { surface } = params

    if (surface && surface.props.textCommands) {
      if (
        !['full', this.name].some(e => {
          return surface.props.textCommands.indexOf(e) !== -1
        }) ||
        surface.props.editing === 'selection'
      ) {
        newState.disabled = true
        return newState
      }
    }

    if (sel && !sel.isNull() && !sel.isCustomSelection() && sel.containerId) {
      newState.disabled = false
    }
    newState.showInContext = this.showInContext(sel, params, context)
    return newState
  }

  createNodeData() {
    return {
      type: 'script',
      language: 'javascript',
      content: '',
    }
  }

  execute(params) {
    super.execute(params)
    const editor = params.surface.context.editor
    editor.emit('codeBlock:created')
  }
}

export default InsertScriptCommand
