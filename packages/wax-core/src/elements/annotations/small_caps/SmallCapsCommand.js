import { AnnotationCommand } from 'substance'

class SmallCapsCommand extends AnnotationCommand {
  isDisabled(sel) {
    if (
      !sel ||
      sel.isNull() ||
      !sel.isAttached() ||
      sel.isCustomSelection() ||
      sel.isNodeSelection() ||
      sel.isContainerSelection()
    ) {
      return true
    }

    if (this.config.disableCollapsedCursor && sel.isCollapsed()) {
      return true
    }

    return false
  }
}

SmallCapsCommand.type = 'small-caps'

export default SmallCapsCommand
