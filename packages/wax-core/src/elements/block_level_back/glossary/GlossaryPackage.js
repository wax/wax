import Glossary from './Glossary'
import GlossaryComponent from './GlossaryComponent'
import GlossaryHTMLConverter from './GlossaryHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'glossary',
  configure: config => {
    config.addNode(Glossary)
    config.addComponent(Glossary.type, GlossaryComponent)
    config.addConverter('html', GlossaryHTMLConverter)

    config.addCommand('glossary', WaxSwitchTextTypeCommand, {
      spec: { type: 'glossary' },
      commandGroup: 'back-matter-a',
    })

    config.addLabel('glossary', {
      en: 'Glossary Entry',
    })
  },
  Glossary: Glossary,
  GlossaryComponent: GlossaryComponent,
  GlossaryHTMLConverter: GlossaryHTMLConverter,
}
