import { AnnotationTool } from 'substance'
import FullScreenCommand from './FullScreenCommand'
import FullScreenTool from './FullScreenTool'

export default {
  name: 'full-screen',
  configure: (config, { disableCollapsedCursor, toolGroup }) => {
    config.addCommand('full-screen-toggle', FullScreenCommand, {
      commandGroup: 'full-screen-control',
    })
    config.addTool('full-screen-toggle', FullScreenTool)

    config.addIcon('full-screen-toggle', { fontawesome: 'fa-arrows-alt' })
    config.addLabel('full-screen-toggle', {
      en: 'Fullscreen',
    })
  },
}
