import InsertEllipsesCommand from './InsertEllipsesCommand'

export default {
  name: 'ellipsis',

  configure: function(config) {
    config.addCommand('insert-ellipsis', InsertEllipsesCommand, {
      commandGroup: 'text-macros',
    })
    config.addKeyboardShortcut('.', {
      type: 'textinput',
      command: 'insert-ellipsis',
    })
  },
}
