import { TextBlockComponent } from 'substance'

class SeriesTitleComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-series-title')
  }
}

export default SeriesTitleComponent
