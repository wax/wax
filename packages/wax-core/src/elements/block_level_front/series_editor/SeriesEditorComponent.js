import { TextBlockComponent } from 'substance'

class SeriesEditorComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-series-editor')
  }
}

export default SeriesEditorComponent
