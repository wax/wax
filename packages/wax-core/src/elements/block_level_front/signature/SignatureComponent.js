import { TextBlockComponent } from 'substance'

class SignatureComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-signature')
  }
}

export default SignatureComponent
