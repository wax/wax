import { platform } from 'substance'

import ShortcutsModalCommand from './ShortcutsModalCommand'
import ShortcutsModalTool from './ShortcutsModalTool'

export default {
  name: 'shortcuts-modal',
  configure: (config, { toolGroup }) => {
    config.addCommand('shortcuts-modal', ShortcutsModalCommand, {
      commandGroup: 'shortcuts-modal',
    })
    config.addTool('shortcuts-modal', ShortcutsModalTool)
    config.addIcon('shortcuts-modal', { fontawesome: ' fa-question' })
    config.addLabel('shortcuts-modal', {
      en: 'Keyboard shortcuts',
    })
    const controllerKey = platform.isMac ? 'cmd' : 'ctrl'

    config.addKeyboardShortcut(`${controllerKey}+o`, {
      description: 'open shortcuts modal',
      command: 'shortcuts-modal',
    })
  },
}
