import { Tool } from 'substance'

let files = []
class InsertImageTool extends Tool {
  getClassNames() {
    return 'sc-insert-image-tool'
  }

  renderButton($$) {
    const button = super.renderButton($$)
    const input = $$('input')
      .attr('type', 'file')
      .attr('id', 'fileName')
      .attr('accept', '.png, .jpg, .jpeg, .svg')
      .ref('input')
      .on('change', this.onFileSelect)
    return [button, input]
  }

  async onClick() {
    files = await this.context.editor.props.assetManager()
    this.onFileSelect(files)
    // this.refs.input.click()
  }

  onFileSelect(files) {
    this.executeCommand({
      files: Array.prototype.slice.call(files),
    })
    // const fileName = document.getElementById('fileName').value
    // const idxDot = fileName.lastIndexOf('.') + 1
    // const extFile = fileName.substr(idxDot, fileName.length).toLowerCase()
    // if (
    //   extFile === 'jpg' ||
    //   extFile === 'jpeg' ||
    //   extFile === 'png' ||
    //   extFile === 'svg'
    // ) {
    //   // const files = files[0]
    // } else {
    //   this.context.editor.emit('displayNotification', function() {
    //     return 'Only jpg/jpeg, png and svg files are allowed!!'
    //   })
    // }
  }
}

export default InsertImageTool
