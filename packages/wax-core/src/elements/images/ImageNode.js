import { DocumentNode } from 'substance'

class ImageNode extends DocumentNode {
  getImageFile() {
    if (this.imageFile) {
      return this.document.get(this.imageFile)
    }
  }

  getUrl() {
    let imageFile = this.getImageFile()
    if (imageFile) {
      return imageFile.getUrl()
    }
  }
}

ImageNode.schema = {
  type: 'image',
  imageFile: { type: 'file' },
  actualFile: { type: 'string', default: '' },
  fileId: { type: 'string', default: '' },
  alt: { type: 'string', default: '' },
  caption: { type: 'string', default: '' },
}

export default ImageNode
