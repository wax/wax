import { includes } from 'lodash'
import { Command } from 'substance'

class ImageCommand extends Command {
  constructor() {
    super({ name: 'insert-image' })
  }

  getCommandState(params) {
    let sel = params.selection
    let surface = params.surface
    let newState = {
      disabled: true,
      active: false,
    }

    if (surface && surface.props.editing === 'selection') {
      newState.disabled = true
      return newState
    }

    if (
      sel &&
      !sel.isNull() &&
      !sel.isCustomSelection() &&
      surface &&
      surface.isContainerEditor() &&
      (includes(surface.props.textCommands, 'full') ||
        includes(surface.props.textCommands, this.name))
    ) {
      newState.disabled = false
    }
    return newState
  }

  execute(params) {
    const { editorSession, files } = params

    editorSession.transaction(tx => {
      files.forEach(file => {
        let imageFile = tx.create({
          type: 'file',
          fileType: 'image',
          mimeType: file.mimetype,
          sourceFile: file,
        })

        const img = tx.insertBlockNode({
          type: 'image',
          imageFile: imageFile.id,
          actualFile: file.source,
          fileId: file.id,
        })

        if (files.length > 1) {
          tx.setSelection({
            type: 'node',
            containerId: 'main',
            nodeId: img.id,
            startOffset: 0,
            endOffset: 0,
          })
          tx.break()
        }
      })
    })

    // editorSession.fileManager.sync()
  }
}

export default ImageCommand
