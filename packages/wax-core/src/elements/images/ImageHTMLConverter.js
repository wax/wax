import { forEach } from 'lodash'
export default {
  type: 'image',
  tagName: 'figure',

  import: function(el, node, converter) {
    const image = el.firstChild
    console.log(image.attr('src'))
    let imageFile = converter.getDocument().create({
      id: 'file-' + node.id,
      type: 'file',
      fileType: 'image',
      url: image.attr('src'),
    })
    node.imageFile = imageFile.id
    node.caption = el.attr('figcaption')
    node.id = el.attr('data-id')
    node.fileId = image.attr('data-fileid')
    node.actualFile = image.attr('src')
  },

  export: function(node, el, exporter) {
    const $$ = exporter.$$
    const captionContainer = node.document.get(`caption-${node.id}`)
    let string = ''

    forEach(exporter.convertContainer(captionContainer), c => {
      string += c.outerHTML
    })

    let imageFile = node.document.get(node.imageFile)
    el.attr('data-id', node.id)
    const fileId = node.fileId
    if (fileId) {
      el.append(
        $$('img')
          .attr('src', node.actualFile)
          .attr('data-fileid', fileId),
      )
    } else {
      el.append($$('img').attr('src', node.actualFile))
    }

    el.append($$('figcaption').html(string))
  },
}
