import { Command } from 'substance'

class EditCaptionCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: true,
    }
    const selection = params.editorSession.getSelection()
    const doc = params.editorSession.getDocument()
    if (selection.type === 'node') {
      const node = doc.get(selection.nodeId)

      if (node.type === 'image') {
        newState = {
          active: true,
          disabled: false,
        }
      }
    }
    return newState
  }
}

export default EditCaptionCommand
