import { forEach } from 'lodash'

import { Tool, documentHelpers, annotationHelpers } from 'substance'

class CustomTagsTool extends Tool {
  render($$) {
    const { editorSession, editor } = this.context
    const selection = editorSession.getSelection()
    const activeCustomTags = this.getCustomTags(editorSession)
    let isDisabled = 'notDisabled'
    if (selection.isCollapsed()) isDisabled = 'disabled'

    const Button = this.getComponent('button')
    let el = $$('div')
      .addClass('sc-custom-tag-tool')
      .append($$('div').addClass('arrow-up'))

    const options = $$('div')
      .addClass('se-options')
      .ref('options')
    forEach(this.context.editor.props.customTags.inline, (tag, index) => {
      let activeClass = ''
      let removeButton = $$('span').addClass('remove-tag')
      forEach(activeCustomTags, activeTag => {
        if (tag.label === activeTag.class) {
          activeClass = 'active'
          removeButton.append(
            $$('div')
              .append('x')
              .attr('title', `remove ${tag.label} from text`)
              .on('click', () => this.removeTag(activeTag)),
          )
        }
      })
      let option = $$('div')
        .addClass('option-container')
        .append(
          $$('button')
            .addClass(`tag-option ${activeClass}`)
            .attr('data-type', tag.label)
            .attr(isDisabled, true)
            .append(
              $$('div')
                .append(tag.label)
                .addClass('option-box')
                .attr('title', `apply tag ${tag.label}`),
            )
            .on('click', () => this.applyTransformation(tag.label)),
        )
        .append(removeButton)
      options.append(option)
    })
    const customTagsList = $$('div').append(options)
    const addNewCustomTag = $$('div')
      .addClass('new-custom-tag-container')
      .append(
        $$('input')
          .attr('id', 'addCustomTag')
          .attr('placeholder', 'add new custom tag')
          .attr('type', 'text')
          .addClass('input'),
      )
      .append(
        $$(Button, {
          label: 'add',
        })
          .addClass('new-custom-tag')
          .on('click', this.andNewTag),
      )

    return el.append(customTagsList, addNewCustomTag)
  }

  andNewTag() {
    const { update } = this.context.editor.props
    const customTag = document.getElementById('addCustomTag').value
    if (customTag.length === 0) return

    const addNewTag = { label: customTag, tagType: 'inline' }

    this.state.allCustomTags.push(addNewTag)

    this.extendState({
      allCustomTags: this.state.allCustomTags,
    })

    this.context.editor.extendProps({
      customTags: {
        inline: this.state.allCustomTags,
        block: this.context.editor.props.customTags.block,
      },
    })

    update({ tags: [addNewTag] })
  }

  getCustomTags(editorSession) {
    const selection = editorSession.getSelection()
    return documentHelpers.getPropertyAnnotationsForSelection(
      editorSession.document,
      selection,
      { type: 'custom-tags' },
    )
  }

  applyTransformation(tag) {
    const { editorSession } = this.context
    const selection = editorSession.getSelection()
    let disabled = false
    let existingCustomTags = []
    const customTags = this.getCustomTags(editorSession)

    //expand cases
    forEach(customTags, tagName => {
      if (
        (tagName.class === tag &&
          selection.start.offset < tagName.start.offset) ||
        (tagName.class === tag &&
          selection.start.offset < tagName.start.offset &&
          selection.end.offset > tagName.end.offset)
      ) {
        existingCustomTags.push(tagName)
      } else if (
        tagName.class === tag &&
        selection.start.offset === tagName.start.offset &&
        selection.end.offset === tagName.end.offset
      ) {
        disabled = true
      }
    })

    if (disabled) return

    if (existingCustomTags.length !== 0) {
      editorSession.transaction(tx => {
        forEach(existingCustomTags, tag => {
          tx.delete(tag.id)
          // annotationHelpers.expandAnnotation(tx, tag, selection)
        })
        this.createTransaction(tx, selection, tag)
      })
    } else {
      editorSession.transaction(tx => {
        this.createTransaction(tx, selection, tag)
      })
    }
  }

  removeTag(tag) {
    const { editorSession } = this.context
    editorSession.transaction(tx => {
      tx.delete(tag.id)
    })
  }

  createTransaction(tx, selection, tag) {
    tx.annotate({ type: 'custom-tags', class: tag })
  }

  getInitialState() {
    const {
      editor: {
        props: {
          customTags: { inline = [], block = [] },
        },
      },
    } = this.context
    return {
      allCustomTags: inline,
    }
  }
}

export default CustomTagsTool
