import CustomTags from './CustomTags'
import CustomTagsTool from './CustomTagsTool'
import CustomTagsCommand from './CustomTagsCommand'
import CustomTagsHTMLConverter from './CustomTagsHTMLConverter'

export default {
  name: 'custom-tags',
  configure: function(config) {
    config.addNode(CustomTags)
    config.addCommand(CustomTags.type, CustomTagsCommand, {
      nodeType: 'custom-tags',
      commandGroup: 'custom-tags',
    })
    config.addTool('custom-tags', CustomTagsTool)
    config.addConverter('html', CustomTagsHTMLConverter)
    // CustomTags.autoExpandRight = false
  },
}
