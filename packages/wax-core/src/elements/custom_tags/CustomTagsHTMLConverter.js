export default {
  type: 'custom-tags',
  tagName: 'custom-inline',

  import: (element, node) => {
    node.class = element
      .attr('class')
      .split('-')
      .join(' ')
  },

  export: (node, element) => {
    element.setAttribute('class', node.class.split(' ').join('-'))
  },
}
