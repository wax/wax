import WaxAnnotationCommand from '../../commands/WaxAnnotationCommand'

class CustomTagsCommand extends WaxAnnotationCommand {
  getCommandState(params, context) {
    // const sel = params.selection
    const sel = this._getSelection(params)
    if (this.isDisabled(sel, params, context)) {
      return {
        disabled: true,
      }
    }

    const annos = this._getAnnotationsForSelection(params)
    const newState = {
      disabled: false,
      active: false,
      mode: null,
    }
    const { surface } = params
    const { editor } = surface.context

    if (!editor.state.toggleInlineTag) {
      newState.disabled = true
      return newState
    }

    if (surface.props.textCommands) {
      if (
        !['full', this.name].some(e => {
          return surface.props.textCommands.indexOf(e) !== -1
        }) ||
        surface.props.editing === 'selection'
      ) {
        newState.disabled = true
        return newState
      }
    }

    if (this.canCreate(annos, sel)) {
      newState.mode = 'create'
    } else if (this.canFuse(annos, sel)) {
      newState.mode = 'fuse'
    } else if (this.canTruncate(annos, sel)) {
      newState.active = true
      newState.mode = 'truncate'
    } else if (this.canExpand(annos, sel)) {
      newState.mode = 'expand'
    } else if (this.canDelete(annos, sel)) {
      newState.active = true
      newState.mode = 'delete'
    } else {
      newState.disabled = true
    }
    newState.showInContext = this.showInContext(sel, params, context)
    return newState
  }
}

export default CustomTagsCommand
