import InsertSingleQuoteMarkCommand from './InsertSingleQuoteMarkCommand'

export default {
  name: 'single-quote-marks',

  configure: function(config) {
    config.addCommand(
      'insert-single-quote-marks',
      InsertSingleQuoteMarkCommand,
      {
        commandGroup: 'text-macros',
      },
    )
    config.addKeyboardShortcut("'", {
      type: 'textinput',
      command: 'insert-single-quote-marks',
    })
  },
}
