import { Command } from 'substance'

class ChangeCaseCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: false,
    }

    const selection = params.editorSession.getSelection()
    // const text = this.getSelectionText()
    // const reg = /^[a-z]+$/i
    //
    // if (text.length === 1 && !reg.test(text)) {
    //   newState.disabled = true
    //   return newState
    // }

    if (
      selection.isCollapsed() ||
      selection.isContainerSelection() ||
      !selection.isPropertySelection()
    ) {
      newState.disabled = true
    }

    if (params.surface) {
      const editorProps = params.surface.context.editor.props
      if (editorProps.editing === 'selection') newState.disabled = true
    }

    return newState
  }

  execute(params) {
    return true
  }

  getSelectionText() {
    let text = ''
    if (window.getSelection) {
      text = window.getSelection().toString()
    } else if (document.selection && document.selection.type != 'Control') {
      text = document.selection.createRange().text
    }
    return text
  }
}

export default ChangeCaseCommand
