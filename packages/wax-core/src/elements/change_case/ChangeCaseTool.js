import { forEach } from 'lodash'
import { Tool, FontAwesomeIcon as Icon, documentHelpers } from 'substance'
import toTitleCase from '../../helpers/titleCase'

class ChangeCaseTool extends Tool {
  constructor(...args) {
    super(...args)
    this.DropDownOptions = [
      {
        name: 'Upper case',
        description: 'tranform all leters to upper case',
        action: 'uppercase',
      },
      {
        name: 'Lower case',
        description: 'tranform all leters to lower case',
        action: 'lowerase',
      },
      {
        name: 'Sentence case',
        description: 'tranform first letter only capped',
        action: 'sentencecase',
      },
      {
        name: 'Title case',
        description: 'tranform first letter of all words capped',
        action: 'titlecase',
      },
    ]
  }

  render($$) {
    const dropDownCarret = $$(Icon, { icon: 'fa-caret-down' }).addClass(
      'icon-down',
    )

    let el = $$('div')
      .addClass('change-case-wrapper')
      .attr('id', 'change-case')
      .append(
        $$('div')
          .addClass('choose-case')
          .attr('title', `Change Case Action`)
          .append('Transform Cases')
          .append(dropDownCarret)
          .on('click', this.toggleDropdown),
      )

    if (this.props.commandState.disabled) {
      el.addClass('sm-disabled')
    }

    let options = $$('span')
    if (this.state.open) {
      el.addClass('sm-open')

      // dropdown options
      options = $$('div')
        .addClass('se-options')
        .ref('options')
      forEach(this.DropDownOptions, (option, index) => {
        let button = $$('button')
          .addClass('se-option')
          .attr('data-type', option)
          .append(
            $$('div')
              .append(option.name)
              .addClass('option-box')
              .attr('title', option.description),
          )
          .on('click', () => this.changeCase(option.action))
        options.append(button)
      })
    }

    return el.append(options)
  }

  toggleDropdown() {
    this.extendState({
      open: !this.state.open,
    })
  }

  changeCase(action) {
    this.toggleDropdown()
    const text = this.getSelectionText()
    const selection = this.context.editorSession.getSelection()
    switch (action) {
      case 'uppercase':
        this.transformToUpper(selection, text)
        break
      case 'lowerase':
        this.transformToLower(selection, text)
        break
      case 'sentencecase':
        this.transformToSentence(selection, text)
        break
      case 'titlecase':
        this.transformToTitle(selection, text)
        break
    }
  }
  transformToUpper(selection, text) {
    this.context.editorSession.transaction(tx => {
      tx.deleteSelection()
      tx.insertText(text.toUpperCase())
    })
  }

  transformToLower(selection, text) {
    this.context.editorSession.transaction(tx => {
      tx.deleteSelection()
      tx.insertText(text.toLowerCase())
    })
  }

  transformToSentence(selection, text) {
    if (
      text.charAt(0).match(/^[a-z]+$/i) !== null &&
      text.charAt(0).match(/^[a-z]+$/i)[0]
    ) {
      this.context.editorSession.transaction(tx => {
        tx.deleteSelection()
        tx.insertText(text.charAt(0).toUpperCase() + text.slice(1))
      })
    } else {
      this.context.editorSession.transaction(tx => {
        tx.deleteSelection()
        tx.insertText(text.charAt(0))
        tx.insertText(text.charAt(1).toUpperCase() + text.slice(2))
      })
    }
  }

  transformToTitle(selection, text) {
    this.context.editorSession.transaction(tx => {
      tx.deleteSelection()
      tx.insertText(text.toTitleCase())
    })
  }

  getSelectionText() {
    let text = ''
    if (window.getSelection) {
      text = window.getSelection().toString()
    } else if (document.selection && document.selection.type != 'Control') {
      text = document.selection.createRange().text
    }
    return text
  }

  getInitialState() {
    return {
      open: false,
    }
  }
}

export default ChangeCaseTool
