import { Command } from 'substance'
import { each } from 'lodash'
import SpellCheckManager from '../spellCheck/SpellCheckManager'

class SpellCheckToggleCommand extends Command {
  getCommandState(params, config) {
    const newState = {
      disabled: false,
      active: false,
    }

    if (params.surface)
      if (params.surface && params.surface.props.spellcheck) {
        newState.active = true
      }
    return newState
  }

  execute(params, context) {
    const selection = params.editorSession.getSelection()
    const surface = context.surfaceManager.getSurface('main')
    const spellCheckStatus = surface.context.editor.state.spellCheck
    params.surface.context.editor.extendState({ spellCheck: !spellCheckStatus })
    params.editorSession.setSelection(selection)

    if (!spellCheckStatus) {
      const { editor } = surface.context
      const spellCheck = new SpellCheckManager(params.editorSession)
      spellCheck.runGlobalCheck()
      editor.emit('displayNotification', () => {
        return `Scanning the document for errors. Please Wait`
      })
    }
    return true
  }
}

SpellCheckToggleCommand.type = 'spell-check-toggle'

export default SpellCheckToggleCommand
