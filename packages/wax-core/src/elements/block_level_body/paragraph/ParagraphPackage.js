import Paragraph from './Paragraph'
import ParagraphComponent from './ParagraphComponent'
import ParagraphHTMLConverter from './ParagraphHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'paragraph',
  configure: function(config) {
    config.addNode(Paragraph)
    config.addComponent(Paragraph.type, ParagraphComponent)
    config.addConverter('html', ParagraphHTMLConverter)

    config.addCommand('paragraph-front', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph' },
      commandGroup: 'front-matter-text',
    })

    config.addCommand('paragraph-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph' },
      commandGroup: 'text-types-part',
    })
    config.addCommand('paragraph-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph' },
      commandGroup: 'text-types-chapter',
    })
    config.addCommand('paragraph-back', WaxSwitchTextTypeCommand, {
      spec: { type: 'paragraph' },
      commandGroup: 'back-matter-a',
    })
    config.addLabel('paragraph-chapter', {
      en: 'General Text',
    })
    config.addLabel('paragraph-part', {
      en: 'General Text',
    })
    config.addLabel('paragraph-front', {
      en: 'General Text',
    })
    config.addLabel('paragraph-back', {
      en: 'General Text',
    })
    // config.addIcon('paragraph', { fontawesome: 'fa-paragraph' })
    // config.addKeyboardShortcut('CommandOrControl+Alt+0', {
    //   command: 'paragraph',
    // })
  },
  Paragraph: Paragraph,
  ParagraphComponent: ParagraphComponent,
  ParagraphHTMLConverter: ParagraphHTMLConverter,
}
