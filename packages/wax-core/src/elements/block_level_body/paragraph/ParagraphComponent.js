import { TextBlockComponent } from 'substance'

class ParagraphComponent extends TextBlockComponent {
  render($$) {
    const el = super.render($$)

    if (this.props.node.content === '') {
      el.addClass('empty-paragraph')
    }
    return el.addClass('sc-paragraph')
  }

  didMount() {
    const { editorSession } = this.context
    editorSession.onRender('document', this.updateClassNames, this)
  }

  updateClassNames() {
    if (this.textContent === '') {
      this.addClass('empty-paragraph')
    } else {
      this.removeClass('empty-paragraph')
    }
  }
}

export default ParagraphComponent
