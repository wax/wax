import { TextBlock } from 'substance'

class PartNumber extends TextBlock {}

PartNumber.type = 'part-number'

export default PartNumber
