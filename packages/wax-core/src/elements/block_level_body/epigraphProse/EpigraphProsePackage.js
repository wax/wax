import EpigraphProse from './EpigraphProse'
import EpigraphProseComponent from './EpigraphProseComponent'
import EpigraphProseHTMLConverter from './EpigraphProseHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'epigraph-prose',
  configure: config => {
    config.addNode(EpigraphProse)
    config.addCommand('epigraph-prose-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'epigraph-prose' },
      commandGroup: 'text-display-part',
    })
    config.addCommand('epigraph-prose-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'epigraph-prose' },
      commandGroup: 'text-display-chapter',
    })
    config.addCommand('epigraph-prose-front', WaxSwitchTextTypeCommand, {
      spec: { type: 'epigraph-prose' },
      commandGroup: 'front-matter-b',
    })
    config.addComponent(EpigraphProse.type, EpigraphProseComponent)
    config.addConverter('html', EpigraphProseHTMLConverter)

    config.addLabel('epigraph-prose-part', {
      en: 'Epigraph: Prose',
    })
    config.addLabel('epigraph-prose-chapter', {
      en: 'Epigraph: Prose',
    })
    config.addLabel('epigraph-prose-front', {
      en: 'Epigraph: Prose',
    })
  },
}
