import ChapterSubtitle from './ChapterSubtitle'
import ChapterSubtitleComponent from './ChapterSubtitleComponent'
import ChapterSubtitleHTMLConverter from './ChapterSubtitleHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'chapter-subtitle',
  configure: config => {
    config.addNode(ChapterSubtitle)
    config.addCommand('chapter-subtitle-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'chapter-subtitle' },
      commandGroup: 'text-display-part',
    })
    config.addCommand('chapter-subtitle-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'chapter-subtitle' },
      commandGroup: 'text-display-chapter',
    })
    config.addCommand('chapter-subtitle-front', WaxSwitchTextTypeCommand, {
      spec: { type: 'chapter-subtitle' },
      commandGroup: 'front-matter-a',
    })
    config.addCommand('chapter-subtitle-back', WaxSwitchTextTypeCommand, {
      spec: { type: 'chapter-subtitle' },
      commandGroup: 'back-matter-display',
    })
    config.addComponent(ChapterSubtitle.type, ChapterSubtitleComponent)
    config.addConverter('html', ChapterSubtitleHTMLConverter)

    config.addLabel('chapter-subtitle-part', {
      en: 'Subtitle',
    })
    config.addLabel('chapter-subtitle-chapter', {
      en: 'Subtitle',
    })
    config.addLabel('chapter-subtitle-front', {
      en: 'Subtitle',
    })
    config.addLabel('chapter-subtitle-back', {
      en: 'Subtitle',
    })
  },
}
