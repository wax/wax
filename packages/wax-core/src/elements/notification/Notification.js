import { Component } from 'substance'

class Notification extends Component {
  constructor(...props) {
    super(...props)
    this.context.editor.on(
      'displayNotification',
      message => this.displayNotification(message),
      this,
    )
  }
  render($$) {
    const empty = $$('span')

    const identifier = $$('div').addClass('identifier')
    const text = $$('div')
      .append(this.state.message)
      .addClass('text')

    const content = $$('div')
      .addClass('content')
      .append(identifier, text)

    const notification = $$('div')
      .addClass('notification')
      .append(content)

    // const number = $$('div')
    //   .addClass('number')
    //   .append($$('p').append('1'))

    if (this.state.notification) return notification
    return empty
  }

  displayNotification(message) {
    this.extendState({ notification: true })
    this.extendState({ message: message() })
    setTimeout(() => {
      this.extendState({ notification: false })
      this.extendState({ message: '' })
    }, 4000)
  }

  getInitialState() {
    return {
      notification: false,
      message: '',
    }
  }
}

export default Notification
