import { Command } from 'substance'
import { each } from 'lodash'

class CustomTagInlineToggleCommand extends Command {
  getCommandState(params, config) {
    const newState = {
      disabled: false,
      active: false,
    }

    if (params.surface)
      if (params.surface && params.surface.props.spellcheck) {
        newState.active = true
      }
    return newState
  }

  execute(params, context) {
    const selection = params.editorSession.getSelection()
    const surface = context.surfaceManager.getSurface('main')
    const customTagInlineStatus = surface.context.editor.state.toggleInlineTag
    const { editor } = surface.context

    editor.extendState({
      toggleInlineTag: !customTagInlineStatus,
    })
    params.editorSession.setSelection(selection)

    const message = !customTagInlineStatus
      ? 'Custom inline tags enabled'
      : 'Custom inline tags disabled'
    editor.emit('displayNotification', () => {
      return message
    })
    return true
  }
}

CustomTagInlineToggleCommand.type = 'custom-tag-inline-toggle'

export default CustomTagInlineToggleCommand
