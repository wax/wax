import { platform, Tool } from 'substance'
import CustomTagInlineToggleCommand from './CustomTagInlineToggleCommand'
import CustomTagInlineToggleTool from './CustomTagInlineToggleTool'

export default {
  name: 'custom-tag-inline-toggle',
  configure: config => {
    config.addCommand(
      'custom-tag-inline-toggle',
      CustomTagInlineToggleCommand,
      {
        commandGroup: 'custom-tag-inline-toggle',
      },
    )
    config.addTool('custom-tag-inline-toggle', CustomTagInlineToggleTool)
    config.addIcon('custom-tag-inline-toggle', { fontawesome: 'fa-check' })
    config.addLabel('custom-tag-inline-toggle', {
      en: 'Toggle custom tag inline',
    })

    let controllerKey = 'ctrl'
    if (platform.isMac) controllerKey = 'cmd'
    //TODO what is the shrotcut?
  },
}
