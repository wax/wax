import { Tool } from 'substance'

class CustomTagInlineToggleTool extends Tool {
  renderButton($$) {
    let button = super.renderButton($$)
    const { editor } = this.context
    const customTagInlineStatus = editor.state.toggleInlineTag
    button.props.active = false
    if (customTagInlineStatus) {
      button.props.active = true
    }
    return button
  }
}

export default CustomTagInlineToggleTool
