import { Tool } from 'substance'

class NoteTool extends Tool {
  renderButton($$) {
    const el = super.renderButton($$)
    el.addClass('toolbar-note')
    const readOnly = this.isSurfaceReadOnly()
    if (readOnly === true) el.attr('disabled', 'true')

    return el
  }

  getSurface() {
    const surfaceManager = this.context.surfaceManager
    const containerId = this.context.editor.props.containerId

    return surfaceManager.getSurface(containerId)
  }

  isSurfaceReadOnly() {
    const surface = this.getSurface()
    if (!surface) return

    return surface.isReadOnlyMode()
  }
}

NoteTool.type = 'note'

export default NoteTool
