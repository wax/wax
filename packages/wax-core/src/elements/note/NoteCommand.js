import { includes } from 'lodash'
import { InsertInlineNodeCommand } from 'substance'

class NoteCommand extends InsertInlineNodeCommand {
  getCommandState(params) {
    const sel = params.selection
    const surface = params.surface
    const newState = {
      disabled: true,
      active: false,
    }

    if (
      sel &&
      !sel.isNull() &&
      !sel.isCustomSelection() &&
      surface &&
      surface.isContainerEditor() &&
      (includes(surface.props.textCommands, 'full') ||
        includes(surface.props.textCommands, this.name))
    ) {
      newState.disabled = false
    }
    return newState
  }

  execute(params) {
    const editor = params.surface.context.editor
    const trackChange = editor.props.trackChanges
    const editorSession = params.editorSession
    const selection = editorSession.getSelection()
    const user = editor.props.user

    if (trackChange) {
      editorSession.transaction(tx => {
        tx.insertInlineNode({
          type: 'note',
        })
        tx.create({
          status: 'add',
          type: 'track-change',
          path: selection.start.path,
          startOffset: selection.start.offset,
          endOffset: selection.end.offset + 1,
          user,
        })
      })
      return
    }

    editorSession.transaction(tx => {
      return tx.insertInlineNode({
        type: 'note',
      })
    })
  }
}

NoteCommand.type = 'note'

export default NoteCommand
