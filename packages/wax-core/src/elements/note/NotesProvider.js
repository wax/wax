import { isEmpty, values } from 'lodash'
import { documentHelpers } from 'substance'
import AbstractProvider from '../../AbstractProvider'

class NotesProvider extends AbstractProvider {
  constructor(document, props) {
    super(document, props)
    this.activeNote = undefined
    this.editorSession.onRender('', this.removeActiveNote, this)
    document.on('document:changed', this.handleDocumentChange, this)
  }

  static get NodeType() {
    return ['note']
  }

  static initialize(context) {
    const config = {
      containerId: context.props.containerId,
      editorSession: context.editorSession,
      name: 'notes',
    }

    return new NotesProvider(context.doc, config)
  }

  removeActiveNote() {
    const focusedSurface = this.getActiveSurface()
    const editorContainerId = this.config.containerId
    const { editorSession } = this
    const selection = editorSession.getSelection()
    const doc = editorSession.getDocument()
    if (typeof this.activeNote === 'undefined') return

    const note = documentHelpers.getPropertyAnnotationsForSelection(
      doc,
      selection,
      { type: 'note' },
    )

    if (note.length === 1) return

    if (focusedSurface && focusedSurface.containerId === editorContainerId) {
      if (typeof this.activeNote !== 'undefined') this.setActiveNote(undefined)
    }
  }

  // TODO -- refactor
  handleDocumentChange(change) {
    const noteCreated = values(change.created).filter(
      value => value.type === 'note',
    )
    const noteDeleted = values(change.deleted).filter(
      value => value.type === 'note',
    )
    const before = values(change.before).filter(value => value.type === 'null')

    const { editorSession } = this
    const doc = editorSession.getDocument()

    // Handle drag and drop
    if (noteCreated[0] && doc.get(`container-${noteCreated[0].id}`)) {
      this.setActiveNote(undefined)
      this.entries = this.computeEntries()
      setTimeout(() => {
        this.setActiveNote(noteCreated[0].id)
        this.emit('note:dragged')
      })
      return
    }

    if (!isEmpty(noteCreated) || !isEmpty(noteDeleted)) {
      // if redo and active note already set remove it
      this.setActiveNote(undefined)
      this.entries = this.computeEntries()
      this.emit('notes:updated', {
        created: noteCreated,
        deleted: noteDeleted,
      })
      if (!isEmpty(noteCreated)) {
        const createdId = noteCreated[0].id
        this.setActiveNote(createdId)
      }
      return
    }
  }

  getActiveNote() {
    return this.activeNote
  }

  setActiveNote(noteId) {
    this.activeNote = noteId
    this.emit('active:changed', noteId)
  }

  // calloutSelected (noteId) {
  //   this.emit('noteSelected', noteId)
  // }
}

export default NotesProvider
