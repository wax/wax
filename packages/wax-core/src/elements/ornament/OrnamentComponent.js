import { Component } from 'substance'

class OrnamentComponent extends Component {
  render($$) {
    const el = $$('div')
      .addClass('ornament-seperator')
      .append('* * *')
    return el
  }
}

export default OrnamentComponent
