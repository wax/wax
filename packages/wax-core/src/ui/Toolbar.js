import { Component, Toolbar as SubstanceToolbar } from 'substance'

// TODO -- should this be a separate package?

class Toolbar extends Component {
  render($$) {
    const {
      commandStates,
      disabled,
      trackChanges,
      trackChangesView,
      configurator,
    } = this.props

    const viewMode = disabled
      ? $$('span')
          .addClass('view-mode')
          .append('Read-Only')
      : ''

    const displayMenu =
      configurator.menus && configurator.menus.topToolBar
        ? configurator.menus.topToolBar
        : 'topDefault'

    return $$('div')
      .addClass('se-toolbar-wrapper')
      .append(
        $$(SubstanceToolbar, {
          commandStates,
          trackChanges,
          trackChangesView,
          toolPanel: configurator.getToolPanel(displayMenu),
        }),
        // .ref('toolbar')
      )
      .append(viewMode)
  }
}

export default Toolbar
