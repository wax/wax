import { Component, ScrollPane } from 'substance'

import ContainerEditor from '../../ContainerEditor'

import './bare.scss'

class Bare extends Component {
  render($$) {
    const {
      configurator,
      containerId,
      editing,
      editorSession,
      trackChanges,
    } = this.props

    // surface
    const editor = $$(ContainerEditor, {
      containerId,
      editing,
      editorSession,
      textCommands: ['full'],
      trackChanges,
    }).ref('main')

    const contentPanel = $$(ScrollPane, {
      name: 'contentPanel',
      scrollbarPosition: 'right',
    })
      .append(editor)
      .attr('id', `content-panel-${containerId}`)
      .ref('contentPanel')

    return $$('div').append(contentPanel)
  }
}

export default Bare
